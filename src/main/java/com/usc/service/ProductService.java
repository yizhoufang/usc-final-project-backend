package com.usc.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usc.beans.Product;
import com.usc.dao.ProductDao;
import com.usc.http.Response;

@Service
@Transactional
public class ProductService {
	@Autowired
	ProductDao productDao;
	
	public Response addProduct(Product product){
		product.setName(product.getName());
		product.setPrice(product.getPrice());
		product.setImage(product.getImage());
		System.out.println(product);
		productDao.save(product);
		return new Response(true);
	}
	
	public Response deleteProduct(int id) {
		if (productDao.findById(id) != null) {
			productDao.deleteById(id);
			return new Response(true);
		} else {
			return new Response(false, "Product is not found!");
		}
	}
}
