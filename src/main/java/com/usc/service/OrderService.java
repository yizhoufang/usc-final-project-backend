package com.usc.service;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import com.usc.beans.Order;
import com.usc.beans.OrderProduct;
import com.usc.beans.Product;
import com.usc.beans.User;
import com.usc.dao.OrderDao;
import com.usc.dao.ProductDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;

@Service
@Transactional
public class OrderService {
	
	@Autowired
	ProductDao productDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	OrderDao orderDao;
	
	public Response placeOrder(Order order, Authentication authentication){
		List<OrderProduct> purchases = order.getPurchases();
		purchases.forEach((orderProduct) -> {
			Product product = productDao.findById(orderProduct.getProduct().getId());
			orderProduct.setProduct(product);
            orderProduct.setOrder(order);
		});
		User user = userDao.findByUsername(authentication.getName());
		order.setUser(user);
        orderDao.save(order);
        order.setPurchases(purchases);
        return new Response(true);
	}
	
	public List<Order> getOrder(Authentication authentication) {
		if(isAdmin(authentication.getAuthorities())) {
            return (List<Order>)orderDao.findAll();
        } else {
            return orderDao.findByUserId(userDao.findByUsername(authentication.getName()).getId());
        }
	}
	
	public boolean isAdmin(Collection<? extends GrantedAuthority> profiles) {
		boolean isAdmin = false;
		for (GrantedAuthority profile : profiles) {
			if (profile.getAuthority().equals("ROLE_ADMIN")) {
				isAdmin = true;
			}
		}
		return isAdmin;
	}
}
