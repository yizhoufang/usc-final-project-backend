package com.usc.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.usc.beans.Address;
import com.usc.beans.User;
import com.usc.dao.AddressDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;

@Service
@Transactional
public class AddressService {
	@Autowired
	AddressDao addressDao;
	
	@Autowired
	UserDao userDao;
	
	public Response addAddress(Address address, Authentication authentication){
		try{
			address.setAddress(address.getAddress());
			address.setCity(address.getCity());
			address.setState(address.getState());
			address.setZipcode(address.getZipcode());
			User user = userDao.findByUsername(authentication.getName());
			address.setUser(user);
			System.out.println(address);
			addressDao.save(address);
			return new Response(true);
		}catch (Exception e) {
            return new Response(false);
        }
		
	}
		
}
