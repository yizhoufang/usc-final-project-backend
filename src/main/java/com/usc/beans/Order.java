package com.usc.beans;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "usc.order")
public class Order {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
	@SequenceGenerator(name = "SEQ", sequenceName = "USC_ORDER_SEQ")
	private int id;
	
	@Column
    private Date purchase_date;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<OrderProduct> purchases;

	public Order() {
		super();
	}

	public Order(int id, Date purchase_date, User user, List<OrderProduct> purchases) {
		super();
		this.id = id;
		this.purchase_date = purchase_date;
		this.user = user;
		this.purchases = purchases;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getPurchase_date() {
		return purchase_date;
	}

	public void setPurchase_date(Date purchase_date) {
		this.purchase_date = purchase_date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<OrderProduct> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<OrderProduct> purchases) {
		this.purchases = purchases;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", purchase_date=" + purchase_date + ", user=" + user + ", purchases=" + purchases
				+ "]";
	}

	
//	public Order() {
//		super();
//	}
//
//	public Order(int id, Date purchase_date, User user, List<OrderProduct> purchases) {
//		super();
//		this.id = id;
//		this.purchase_date = purchase_date;
//		this.user = user;
//		this.purchases = purchases;
//	}
//
//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}
//
//	public Date getPurchase_date() {
//		return purchase_date;
//	}
//
//	public void setPurchase_date(Date purchase_date) {
//		this.purchase_date = purchase_date;
//	}
//
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//	public List<OrderProduct> getPurchases() {
//		return purchases;
//	}
//
//	public void setPurchases(List<OrderProduct> purchases) {
//		this.purchases = purchases;
//	}
//
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}
//
//	@Override
//	public String toString() {
//		return "Order [id=" + id + ", purchase_date=" + purchase_date + ", user=" + user + ", purchases=" + purchases
//				+ "]";
//	}
	
	
}
