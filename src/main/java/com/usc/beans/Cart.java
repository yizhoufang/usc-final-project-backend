//package com.usc.beans;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToMany;
//import javax.persistence.ManyToOne;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "usc.cart")
//public class Cart {
//	private static final long serialVersionUID = 1L;
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
//	@SequenceGenerator(name = "SEQ", sequenceName = "USC_CART_SEQ")
//	private int id;
//	
//	@ManyToOne
//	@JoinColumn(name = "user_id", referencedColumnName = "id")
//	private User user;
//	
//	@ManyToOne
//	@JoinColumn(name = "product", referencedColumnName = "id")
//	private Product product;
//
//	public Cart() {
//		super();
//	}
//
//	public Cart(int id, User user, Product product) {
//		super();
//		this.id = id;
//		this.user = user;
//		this.product = product;
//	}
//
//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}
//
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//	public Product getProduct() {
//		return product;
//	}
//
//	public void setProduct(Product product) {
//		this.product = product;
//	}
//
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}
//
//	@Override
//	public String toString() {
//		return "Cart [id=" + id + ", user=" + user + ", product=" + product + "]";
//	}
//	
//	
//}
