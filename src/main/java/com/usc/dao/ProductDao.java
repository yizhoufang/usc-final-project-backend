package com.usc.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.beans.Product;

public interface ProductDao extends JpaRepository<Product, Integer> {
	Product findById(int id);
}
