//package com.usc.dao;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import com.usc.beans.Cart;
//import com.usc.beans.User;
//
//public interface CartDao extends JpaRepository<Cart, Integer> {
//	Cart findByUser(User user);
//	Cart findByUserId(int userid);
//}
