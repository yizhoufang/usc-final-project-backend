package com.usc.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.beans.OrderProduct;

public interface OrderProductDao extends JpaRepository<OrderProduct, Integer> {

}
