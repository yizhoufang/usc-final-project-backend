package com.usc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.usc.beans.Address;
import com.usc.beans.User;

@Repository
public interface AddressDao extends JpaRepository<Address, Integer> {
	Address findByUser(User user);
	
}
