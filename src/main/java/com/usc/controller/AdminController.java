package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Order;
import com.usc.beans.Product;
import com.usc.beans.User;
import com.usc.dao.OrderDao;
import com.usc.dao.ProductDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;
import com.usc.service.ProductService;
import com.usc.service.UserService;

@RestController() // recept API request
@RequestMapping("/admin")
public class AdminController {

//	@Autowired
//	OrderDao orderDao;
//	
//	@Autowired
//	OrderService orderService;
//	
//	@Autowired
//	UserDao userDao;
//
//	@Autowired
//	UserService userService;
//	
//	@Autowired
//	ProductDao productDao;
//	
//	@Autowired
//	ProductService productService;
//	
//	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
//	@GetMapping("/users")
//	public List<User> getusers() {
//		return userDao.findAll();
//	}
//	
//	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
//	@GetMapping("/orders")
//	public List<Order> getorders() {
//		return orderDao.findAll();
//	}
//	
//	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
//	@GetMapping("/products")
//	public List<Product> getproducts() {
//		return productDao.findAll();
//	}
//	
//	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
//	@GetMapping("/addProduct")
//	public Response addProduct(@RequestBody Product product) {
//		return productService.addProduct(product);
//	}
//	
//	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
//	@DeleteMapping("/deleteProduct/{id}")
//	public Response deleteUser(@PathVariable int id) {
//		return userService.deleteUser(id);
//	}
	
}
