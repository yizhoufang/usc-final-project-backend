package com.usc.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Address;
import com.usc.beans.User;
import com.usc.dao.AddressDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;
import com.usc.service.AddressService;
import com.usc.service.UserService;

@RestController() // recept API request
@RequestMapping("/users")
public class UserController {
	@Autowired
	UserDao userDao;

	@Autowired
	UserService userService;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	AddressDao addressDao;
	
	@Autowired
	AddressService addressService;

	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping
	public List<User> getusers() {
		return userDao.findAll();
	}

	@PostMapping
	public Response addUser(@RequestBody User user) { // json
		return userService.register(user);
	}

	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@PutMapping
	public Response changeUser(@RequestBody User user, Authentication authentication) {
		return userService.changePassword(user, authentication);
	}

	@DeleteMapping("/{id}")
	public Response deleteUser(@PathVariable int id) {
		return userService.deleteUser(id);
	}
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	@PostMapping("/addAddress")
	public Response addAddress(@RequestBody Address address, Authentication authentication){
		return addressService.addAddress(address, authentication);
	}
	
//	@GetMapping("/cart")
//	public List<Cart> getcart(Authentication authentication) {
//		List<Cart> cart = new ArrayList<>();
//		
//		return cartDao.findByUserId(userDao.findByUsername(authentication.getName()).getId());
//	}
	
	@GetMapping("isAdmin")
	public boolean isAdmin(Authentication authentication){
		return userService.isAdmin(authentication.getAuthorities());
	}
	
}

