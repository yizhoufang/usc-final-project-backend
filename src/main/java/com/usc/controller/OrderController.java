package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.Order;
import com.usc.dao.OrderDao;
import com.usc.http.Response;
import com.usc.service.OrderService;

@RestController() // recept API request
@RequestMapping("/order")
public class OrderController {
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	OrderService orderService;
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_USER')")
	@GetMapping()
	public List<Order> getOrder(Authentication authentication){
		return orderService.getOrder(authentication);
	}
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_USER')")
	@PostMapping()
	public Response addOrder(@RequestBody Order order, Authentication authentication) {
		return orderService.placeOrder(order, authentication);
	}
}
