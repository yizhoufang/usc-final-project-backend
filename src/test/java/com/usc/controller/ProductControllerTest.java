package com.usc.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;

import com.usc.beans.Product;
import com.usc.dao.ProductDao;
import com.usc.http.Response;
import com.usc.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {
    @Mock
    private ProductService productService;
    @Mock
    private ProductDao productDao;
    @InjectMocks
    private ProductController productController;

    @Test
    @WithMockUser(roles = "ROLE_ADMIN")
    public void testAddProduct() {
        Product product = new Product(1, "Test Product", "1", "image-url");
        Mockito.when(productService.addProduct(product)).thenReturn(new Response(true));
        Response response = productController.addProduct(product);
        Assert.assertEquals(200, response.getCode());
        Mockito.verify(productService).addProduct(product);
    }
    
	@Test
	public void testGetProducts() {
		Mockito.when(productDao.findAll()).thenReturn(Arrays.asList(new Product()));
		List<Product> products = productController.getproducts();
		Assert.assertEquals(1, products.size());
		Mockito.verify(productDao).findAll();
	}

}
