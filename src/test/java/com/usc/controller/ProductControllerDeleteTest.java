package com.usc.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.test.context.support.WithMockUser;

import com.usc.beans.Product;
import com.usc.dao.ProductDao;
import com.usc.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerDeleteTest {
	@InjectMocks
    private ProductService productService;
    @Mock
    private ProductDao productDao;
    @Mock
    private ProductController productController;
    
    @Test
    @WithMockUser(roles = "ROLE_ADMIN")
	public void testDeleteProduct() {
		Product product = new Product(1, "Test Product", "1", "image-url");
		Mockito.when(productDao.findById(product.getId())).thenReturn(new Product());
		productService.deleteProduct(1);
		Mockito.verify(productDao).deleteById(1);
	}
}
